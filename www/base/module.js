define(function(require) {
     var v1 = require('base/openapi');
     var v2 = require('base/util');
     return {
         'openapi': v1,
         'util': v2
      };
});