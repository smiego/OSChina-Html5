define(function(require) {
     var v1 = require('demo2/index');
     var v2 = require('demo2/login');
     return {
         'index': v1,
         'login': v2
      };
});