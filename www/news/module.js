define(function(require) {
     var v2 = require('news/blog-list');
     var v3 = require('news/index');
     var v4 = require('news/news-blog-detail');
     var v5 = require('news/news-detail');
     var v6 = require('news/news-list');
     var v7 = require('news/news-search');
     var v8 = require('news/recommend-list');
     return {
         'blog-list': v2,
         'index': v3,
         'news-blog-detail': v4,
         'news-detail': v5,
         'news-list': v6,
         'news-search': v7,
         'recommend-list': v8
      };
});