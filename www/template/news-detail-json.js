{
	"id": 26746,
	"body": "
 

\n
MongoDB 2.4 正式版发布了，改版值得关注改进包括：

\n
\n
基于哈希的分区 Hash-based Sharding
\n
数组封顶 Capped Arrays
\n
全文搜索 Text Search (Beta)
\n
地理空间特性的增强 Geospatial Enhancements
\n
更快的计数 Faster Counts
\n
工作集合分析器 Working Set Analyzer
\n
V8 JavaScript 引擎
\n
安全性提升 Security
\n
\n
该版本包含 692 项的改进和提升，详细的改进记录请看这里。

\n
而 MongoDB 团队目前已经开始 MongoDB 2.6 版本的开发。

\n
 

",
	"pubDate": "2013-03-22 11:07:25.0",
	"author": "oschina",
	"title": "MongoDB 2.4 正式版发布，支持全文搜索",
	"authorid": 4311,
	"relativies": [{
		"title": "Apache 2.4 真的比Nginx 快吗？",
		"url": "http://www.oschina.net/news/26717"
	}, {
		"title": "MongoDB 2.0.3 发布",
		"url": "http://www.oschina.net/news/26156/mongodb-2-0-3"
	}, {
		"title": "Apache 2.4 发布，直接瞄准 Nginx ",
		"url": "http://www.oschina.net/news/25947/apache-http-server-2-4-0-aim-at-nginx"
	}, {
		"title": "MongoDB 最佳实践",
		"url": "http://www.oschina.net/question/12_38878"
	}, {
		"title": "Django 1.4 beta 1 发布，不支持 Python 2.4",
		"url": "http://www.oschina.net/news/25811/django-14-beta-1-released"
	}, {
		"title": "PHP Shell 2.4 发布，PHP封装的Web工具",
		"url": "http://www.oschina.net/news/25793/php-shell-2-4-released"
	}, {
		"title": "Spring Data MongoDB 1.0.1 GA ",
		"url": "http://www.oschina.net/news/25755/spring-data-mongodb-1-0-1-ga"
	}, {
		"title": "lambdaj 2.4 发布，Java 集合工具包",
		"url": "http://www.oschina.net/news/25706/lambdaj-2-4-released"
	}, {
		"title": "Lift 2.4 发布，Scala 的 Web 框架",
		"url": "http://www.oschina.net/news/25577/lift-2-4-released"
	}, {
		"title": "Calenco 2.4 发布，Web协助编辑平台",
		"url": "http://www.oschina.net/news/25255/calenco-2-4-released"
	}],
	"notice": {
		"replyCount": 0,
		"msgCount": 0,
		"fansCount": 0,
		"referCount": 0
	},
	"favorite": 0,
	"commentCount": 0,
	"url": ""
}