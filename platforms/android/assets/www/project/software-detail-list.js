define(['text!project/software-detail-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'project-software-detail-list',
			events: {
				"click .backBtn": "goBack",
				"click .projectList": "goToTagList",
			},
			goBack: function() {
				history.back();
			},
			goToTagList: function(el) {
				var $target = $(el.currentTarget);
				var tag = $target.attr("data-tag");
				var name = $target.attr("data-name");
				name = encodeURI(name);
				this.navigate("software-tag-list?tag=" + tag + "&name=" + name, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var tag = Util.request("tag");
				var name = Util.request("name");
				name = decodeURI(name);
				$("#projectTitle").html('');
				$("#projectTitle").html(name);
				Util.loadList(this, 'project-software-detail-list', OpenAPI.project_catalog_list, {
					'tag': tag,
					'dataType': OpenAPI.dataType
				},true);
				//write your business logic here :)
			}
		}); //view define

	});