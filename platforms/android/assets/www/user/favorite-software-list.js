define(['text!user/favorite-software-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'project-favorite-software-list',
			events: {
				"click .backBtn": "goBack",
				"click .favoriteList": "goToSoftDetail",
			},
			goBack: function() {
				this.navigate("user-info", {
					trigger: true
				});
			},
			goToSoftDetail: function(el) {
				var $target = $(el.currentTarget);
				var url = $target.attr("data-url");
				this.navigateModule("project/software-detail?url=" + url, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.loadList(this, 'user-favorite-software-list', OpenAPI.favorite_list, {
					'type': 1,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': access_token,
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});