define(['text!my/critic-list.html', "../base/openapi", '../base/util', "../base/login/login", "../tweet/tweet-list"],
	function(viewTemplate, OpenAPI, Util, Login, TweetList) {
		return Piece.View.extend({
			tweetList: null,
			login: null,
			id: 'my_critic-list',
			events: {
				"click .editBtn": "editQuestion",
				"click .tweetContent": "tweetDetail",
				"click .tweetListImg": 'goToUserInfor'
			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
			},
			editQuestion: function() {
				tweetList = new TweetList();

				tweetList.navigate("tweet/tweet-issue?from=critic-list", {
					trigger: true
				});
			},
			tweetDetail: function(el) {
				tweetList = new TweetList();

				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				tweetList.navigate("tweet/tweet-detail?id=" + id, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				login = new Login();

				var checkLogin = Util.checkLogin();

				if (checkLogin === false) {
					login.show();
				} else {
					var me = this;
					var user_message = Piece.Store.loadObject('user_message');
					var user_token = Piece.Store.loadObject('user_token');
					var criticPrompt = $('.criticPrompt').html();
					if (criticPrompt != '') {
						Util.AjaxNoLoading(OpenAPI.clear_notice, "GET", {
							access_token: user_token.access_token,
							type: 3,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							var NavPrompt = Piece.Session.loadObject('Prompt');
							if (NavPrompt != null) {
								NavPrompt.replyCount = 0;
								Piece.Session.saveObject('Prompt', NavPrompt);
							}

							Util.loadList(me, 'my-critic-list', OpenAPI.my_list, {
								'catalog': 3,
								'access_token': user_token.access_token,
								'user': user_message.id,
								'pageSize': OpenAPI.pageSize,
								'page': 1,
								'dataType': OpenAPI.dataType
							});
						}, null, null);
					} else {
						Util.loadList(me, 'my-critic-list', OpenAPI.my_list, {
							'catalog': 3,
							'access_token': user_token.access_token,
							'user': user_message.id,
							'pageSize': OpenAPI.pageSize,
							'page': 1,
							'dataType': OpenAPI.dataType
						});
					}



				}
			}
		}); //view define

	});